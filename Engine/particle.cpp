#include "particle.h"


particle::particle()
{
	x=0;
	y=0;
	z=0;
	edge=1.0;
}

void particle::animate(float dt)
{

	life_time=life_time- dt;
	position.addV(speed.multf(dt));

}

void particle::display()
{
	
	glPushMatrix();
	glTranslatef(position.x,position.y,0);
	

		glBegin(GL_QUADS);
    		glColor3d(color.x,color.y,color.z);
      		glVertex3f(x,y,z);
      		glVertex3f(x+edge,y,z);
      		glVertex3f(x+edge,y-edge,z);
      		glVertex3f(x,y-edge,z);
        
  		glEnd();


  	glPopMatrix();
}
