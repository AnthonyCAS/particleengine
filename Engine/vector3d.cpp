#include "vector3d.h"
myvector3d::myvector3d()
{
	
}


void myvector3d::set(float n_x, float n_y, float n_z)
{
	x=n_x; 
	y=n_y;
	z=n_z;	
}


void myvector3d::add(float n_x, float n_y, float n_z)
{
	x+=n_x;
	y+=n_y;
	z+=n_z;
}

void myvector3d::addV(myvector3d other)
{
	x+=other.x;
	y+=other.y;
	z+=other.z;
}

myvector3d myvector3d::divf(float mass)
{
	myvector3d out;
	out.x=x/mass;
	out.y=y/mass;
	out.z=z/mass;
	return out;
}

myvector3d myvector3d::multf(float dt)
{
	myvector3d out;
	out.x=x*dt;
	out.y=y*dt;
	out.z=z*dt;
	return out;
}


void myvector3d::print()
{

	cout<<x<<" "<<y<<" "<<z<<endl;
}