
#include <math.h>
#include <iostream>
#include "vector3d.h"

#include <GL/glut.h>

using namespace std;


class particle 
{
	 public:
	 	particle();
	 	float life_time;
	 	float x, y,z;
	 	float edge;

	 	void animate(float dt);
	 	void display();
	 	myvector3d speed;
	 	myvector3d color;
	 	myvector3d position;
	 	



};
