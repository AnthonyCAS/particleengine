#include "motorparticle.h"


MotorParticle::MotorParticle(float x, float y, int num_p)
{
	num_particles=num_p;
	cout<<num_particles<<"par"<<endl;
	
	for(int i; i<num_particles; i++)
	{
		particle particle;
		particle.life_time=randomFloat(2,4);
		particle.speed.set(randomFloat(-1,1),randomFloat(-1,1),0.0);
		particle.color.set(255, 55, 0);
		particle.position.set(x, y, 0);	
		vec.push_back(particle);
	}
	

}

void MotorParticle::animate(float dt)
{


	for (int i = 0; i < num_particles; ++i)
	{
		vec[i].animate(dt);
		if(vec[i].life_time <=0)
			vec.erase(vec.begin()+(i));
	}


} 

void MotorParticle::display()
{
	for (int i = 0; i < num_particles; ++i)
	{
		vec[i].display();
	}
}

float MotorParticle::randomFloat(float init, float end)
{
	srand (static_cast <unsigned> (time(0)));
    return init + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(end-init)));
}