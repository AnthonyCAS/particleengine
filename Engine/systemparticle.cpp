#include <math.h>
#include <iostream>
#include <vector>

#include <GL/glut.h>
#include "motorparticle.h"

using namespace std;

//varibales
#define RED 0
#define GREEN 0
#define BLUE 0
#define ALPHA 1

int width = 300, height=600;
//float player_x=0, player_y=-60,player_z=0;

bool bdown=false, bup=false, bleft = false,bright = false;
float dt=0;

float mousex=0,mousey=0;


vector<MotorParticle> vec_motors;




GLvoid window_redraw(GLsizei width, GLsizei height);
void init_GL(void);
GLvoid window_display();
GLvoid callback_mouse(int button, int state, int x, int y);
GLvoid callback_motion(int x, int y);
GLvoid callback_special(int key, int x, int y);
GLvoid callback_special_up(int key, int x, int y);
GLvoid window_idle();


int main(int argc, char** argv) {

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize( width, height);
	glutCreateWindow("Particles"); //titulo de la ventana

	init_GL(); //fucnion de inicializacion de OpenGL

	glutDisplayFunc(&window_display);	 
	glutReshapeFunc(&window_redraw);
	//glutSpecialFunc(&callback_special);	
	//glutSpecialUpFunc(&callback_special_up);
  glutMouseFunc(&callback_mouse);	
  glutMotionFunc(&callback_motion);
  glutIdleFunc(&window_idle);
	glutMainLoop(); //bucle de rendering

		return 0;
}



GLvoid window_redraw(GLsizei width, GLsizei height) {
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	glOrtho(-30.0f,  30.0f,-80.0f, 80.0f, -10.0f, 10.0f); 
	// todas la informaciones previas se aplican al la matrice del ModelView
	glMatrixMode(GL_MODELVIEW);
}




void init_GL(void) {
	//Color del fondo de la escena
	GLfloat position[] = { 0.0f, 5.0f, 10.0f, 0.0 };
/*
  speed.set(0.0,0.0,0.0);
  position_player.set(0.0,-60.0 ,0.0);
  */

  //enable light : try without it
  glLightfv(GL_LIGHT0, GL_POSITION, position);
  glEnable(GL_LIGHTING);
  //light 0 "on": try without it
  glEnable(GL_LIGHT0);

  //shading model : try GL_FLAT
  glShadeModel(GL_SMOOTH);

  glEnable(GL_DEPTH_TEST);

  //enable material : try without it
  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  glClearColor(RED, GREEN, BLUE, ALPHA);
}


GLvoid window_display()
{

  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-30.0f, 30.0f, -80.0f, 80.0f, -10.0f, 10.0f);
 
/*
  
  motor.animate(dt)
  
  motor.display()
  */

  //dibujar motor 
  MotorParticle motor(mousex,mousey,10);
  motor.animate(0.01);
  motor.display();


  glutSwapBuffers();

  glFlush();
}



GLvoid callback_special(int key, int x, int y)
{
  switch (key)
  {
  case GLUT_KEY_UP:
    bup=true;
    glutPostRedisplay();      // et on demande le réaffichage.
    break;

  case GLUT_KEY_DOWN:
    bdown=true;
    glutPostRedisplay();      // et on demande le réaffichage.
    break;

  case GLUT_KEY_LEFT: 
  	bleft=true;        
    glutPostRedisplay();      // et on demande le réaffichage.
    break;

  case GLUT_KEY_RIGHT:  
    bright=true;      
    glutPostRedisplay();      // et on demande le réaffichage.
    break;
  }
}

GLvoid callback_special_up(int key, int x, int y)
{
  switch (key)
  {
  case GLUT_KEY_UP:
    bup=false;
    glutPostRedisplay();      // et on demande le réaffichage.
    break;

  case GLUT_KEY_DOWN:
    bdown=false;
    glutPostRedisplay();      // et on demande le réaffichage.
    break;

  case GLUT_KEY_LEFT: 
  	bleft=false;        
    glutPostRedisplay();      // et on demande le réaffichage.
    break;

  case GLUT_KEY_RIGHT:  
    bright=false;      
    glutPostRedisplay();      // et on demande le réaffichage.
    break;
  
  }
}

//function called on each frame
GLvoid window_idle()
{


  glutPostRedisplay();
}



GLvoid callback_mouse(int button, int state, int x, int y)
{

  if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)
  { 
     if(x<width/10)
      {
        x=x*(-1);
      }

      if(y>=height/10)
      {
        y=(y%height/10)*(-1);
      }
      if(x>=width/10)
      {
        x=x%width/10;
      }
      mousex=(float)x;
        mousey=(float)y;
      cout<<x<<" "<<y;
  }
}

GLvoid callback_motion(int x, int y)
{
 /* delta_y=y-mousey;
  delta_x=x-mousex;      
      cout<<delta_x<<" "<<delta_y<<endl;
      */
  glutPostRedisplay();            
}